﻿using MultiLang;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiLangTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = ml.ml_string(3,@"Multiple

lines

of

text.");
            label2.Text = ml.ml_string(5,"Multiple\r\n\r\nlines\r\n\r\nof\r\n\r\ntext.");
        }
    }
}
